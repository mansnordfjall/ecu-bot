#!/bin/bash

function log() {
    echo "[`date +\"%d/%m/%Y %H:%M:%S\"`] $@" >> logs/upgrade-log
}

> logs/upgrade-log; log "Starting upgrade"

# Get latest version
log "Getting latest version"
git fetch origin master
log `git pull origin master`

# Install dependencies
log "Installing dependencies"
npm install

log "Upgrade completed"
