const { join } = require('path');

const TOKEN = process.env.ECU_BOT_TOKEN;
const Client = require('./structs/Client');

const config = require('../config.json');

const client = new Client({
  config,
  commandDirectory: join(__dirname, 'commands'),
  commandPrefix: '!',
  commandGroupNames: {
    util: 'Utillity',
    fun: 'Fun',
    tags: 'Tags',
  },
}, { disabledEvents: ['TYPING_START', 'TYPING_STOP'] });

/* Error Handling */
client.on('error', (err) => {
  client.logger.error(`Client websocket error: ${err.message}\n${err.stack}`);
});

process.on('unhandledRejection', (reason, promise) => client.logger.error(`Unhandled rejection "${promise}": "${reason.message}"`));

client.login(TOKEN);
