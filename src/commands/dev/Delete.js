const Command = require('../../structs/Command');

class DeleteCommand extends Command {
  constructor(handler) {
    super(handler, 'delete', {
      aliases: ['clear'],
      description: 'Deletes X messages in a channel (Max 100).',
      guildOnly: true,
      ownerOnly: true,
      hidden: true,
      args: [
        {
          name: 'amount',
          type: 'number',
          optional: true,
        },
      ],
    });
  }

  async invoke(message, args) {
    let { amount } = args;
    if (!amount) amount = 50;
    else if (amount > 100) amount = 100;

    const msgs = await message.channel.fetchMessages({ limit: amount });
    const removed = await message.channel.bulkDelete(msgs, true);
    const msg = await message.channel.send(`Removed \`${removed.size}\` messages from this channel.`);
    msg.delete(5000);
  }
}

module.exports = DeleteCommand;
