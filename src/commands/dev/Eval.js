const { inspect } = require('util');
const Command = require('../../structs/Command');

class EvalCommand extends Command {
  constructor(handler) {
    super(handler, 'eval', {
      hidden: true,
      ownerOnly: true,
      args: [
        {
          name: 'code',
          type: 'string',
          remainder: true,
        },
      ],
    });
  }

  redactOutput(output) {
    const filter = new RegExp(`${this.client.token}`, 'g');
    const inspected = inspect(output, { depth: 1, maxArrayLength: null });
    const final = inspected.replace(filter, '[REDACTED]');
    return final;
  }

  invoke(message, args) {
    const { code } = args;
    try {
      const startTime = process.hrtime();
      // eslint-disable-next-line no-eval
      let result = eval(code);
      const elapsed = process.hrtime(startTime)[1] / 1000000;

      const type = typeof result;
      result = this.redactOutput(result);

      const inputStr = `\`\`\`js\n${code}\n\`\`\``;
      const outputStr = `\`\`\`js\n${result}\n\`\`\``;

      const finalStr = `:inbox_tray: **Input** ${inputStr}`
        + `\n:outbox_tray: **Output** - **Type**: \`${type}\` ${outputStr}`
        + `\n*Executed in \`${elapsed.toFixed(4)}ms\`*`;

      message.channel.send({
        embed: this.client.embed({
          title: 'Evaluate',
          description: finalStr,
        }),
      });
    } catch (err) {
      message.channel.send(`\`\`\`js\n${err.toString()}\n\`\`\``);
    }
  }
}

module.exports = EvalCommand;
