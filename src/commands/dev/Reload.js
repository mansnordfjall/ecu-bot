/* eslint-disable no-underscore-dangle */
const { Collection } = require('discord.js');
const Command = require('../../structs/Command');

class ReloadCommand extends Command {
  constructor(handler) {
    super(handler, 'reload', {
      description: 'Reload a command (only available to bot owner)',
      ownerOnly: true,
      guildOnly: false,
      hidden: true,
      args: [
        {
          name: 'command',
          type: 'string',
          optional: true,
        },
      ],
    });
  }

  invoke(message, args) {
    if (!args.command) {
      this.handler.registry.commands = new Collection();
      this.handler.registry.aliases = new Collection();

      this.handler.loadCommands();
      message.reply('Successfully reloaded all commands!');
      return undefined;
    }

    const command = this.handler.registry.get(args.command);

    if (!command) {
      message.reply(`Couldn't find a command with the name \`${args.command}\`.`);
      return undefined;
    }

    const success = command.reload();
    if (!success) {
      this.handler.registry.add(command);
      message.reply(`Failed to reload \`${command.name}\` command, no changes were made.`);
    } else message.reply(`Successfully reloaded \`${command.name}\` command!`);

    return undefined;
  }
}

module.exports = ReloadCommand;
