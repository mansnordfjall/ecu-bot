// const fetch = require('node-fetch');
const { spawn, exec } = require('child_process');
const Command = require('../../structs/Command');

class PingCommand extends Command {
  constructor(handler) {
    super(handler, 'upgrade', {
      group: 'dev',
      guildOnly: false,
      ownerOnly: true,
      args: [
        {
          name: 'changes',
          optional: true,
        },
      ],
    });
  }

  invoke(message, args) {
    const { changes } = args;

    if (changes) {
      exec('git fetch && git whatchanged origin/stable -n 1', (err, stdout) => {
        if (err) {
          message.channel.send(`Something went wrong: \n\`\`\`\n${err}\n\`\`\``);
        }
        message.channel.send(`\`\`\`\n${stdout}\n\`\`\``);
      });
      return undefined;
    }

    const childProc = spawn('sh', ['scripts/upgrade.sh'], { detached: true });
    childProc.unref();
    process.exit(0);

    return undefined;
  }
}

module.exports = PingCommand;
