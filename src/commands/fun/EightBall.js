const Command = require('../../structs/Command');

class EightBallCommand extends Command {
  constructor(handler) {
    super(handler, '8ball', {
      group: 'fun',
      aliases: ['choose'],
      description: 'Returns one of multiple preset values or from values provided.',
      examples: ['8ball', '8ball eat, sleep, code'],
      args: [
        {
          name: 'values',
          type: 'list',
          optional: true,
          remainder: true,
        },
      ],
    });

    this.balls = [
      'You should get some sleep.',
      'I think you should get something to eat.',
    ];
  }

  invoke(message, args) {
    const { values } = args;
    let selectedValue;

    if (values) { // user gave us values to work with
      const randIndex = Math.floor(Math.random() * values.length);
      selectedValue = values[randIndex];
    } else {
      const randIndex = Math.floor(Math.random() * this.balls.length);
      selectedValue = this.balls[randIndex];
    }

    if (selectedValue) message.channel.send(`> ${selectedValue}`);
    else message.channel.send('Something went wrong, could not find a valid value!');
  }
}

module.exports = EightBallCommand;
