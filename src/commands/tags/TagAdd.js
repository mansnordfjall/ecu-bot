const Command = require('../../structs/Command');

class TagsCommand extends Command {
  constructor(handler) {
    super(handler, 'tagadd', {
      group: 'tags',
      description: 'Add a new tag to the tags list.',
      examples: ['tagadd gogl https://google.com/'],
      args: [
        {
          name: 'name',
        },
        {
          name: 'content',
          remainder: true,
        },
      ],
    });
  }

  invoke(message, args) {
    const { name, content } = args;

    if (this.registry.commands.has(name)) {
      message.channel.send('There is a command with this name. Try a different one!');
      return undefined;
    }

    const wasAdded = this.client.tagRegistry.add(name, content);
    if (!wasAdded) {
      message.channel.send(`A tag with the name \`${name}\` already exists!`);
      return undefined;
    }

    message.channel.send(`Added tag with name \`${name}\`.`);
    return undefined;
  }
}

module.exports = TagsCommand;
