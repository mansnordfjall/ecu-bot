const Command = require('../../structs/Command');

class TagsCommand extends Command {
  constructor(handler) {
    super(handler, 'tagdel', {
      group: 'tags',
      description: 'Removes a tag with given name.',
      examples: ['tagdel gogl'],
      args: [
        {
          name: 'name',
        },
      ],
    });
  }

  invoke(message, args) {
    const { name } = args;

    const wasRemoved = this.client.tagRegistry.remove(name);
    if (!wasRemoved) {
      message.channel.send(`Could not find a tag with the name \`${name}\` to remove.`);
      return undefined;
    }

    message.channel.send(`Removed a tag with the name \`${name}\`!`);
    return undefined;
  }
}

module.exports = TagsCommand;
