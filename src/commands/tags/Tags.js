const Command = require('../../structs/Command');

class TagsCommand extends Command {
  constructor(handler) {
    super(handler, 'tags', {
      group: 'tags',
      description: 'Display the available tags.',
    });
  }

  invoke(message) {
    const tags = Object.keys(this.client.tagRegistry.tags);
    const str = tags.map((tag) => `\`${tag}\``).join(', ');
    const embed = this.client.embed({
      title: 'Available Tags',
      description: str,
    });
    message.channel.send('', { embed });
  }
}

module.exports = TagsCommand;
