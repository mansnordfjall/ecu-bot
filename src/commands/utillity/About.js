const Command = require('../../structs/Command');
const Helper = require('../../util/Helper');

class AboutCommand extends Command {
  constructor(handler) {
    super(handler, 'about', {
      group: 'util',
      aliases: ['info'],
      description: 'Shows information about this bot.',
    });
  }

  invoke(message) {
    const uptime = Helper.formatTime(process.uptime());
    const used = process.memoryUsage().heapUsed / 1024 / 1024;

    const embed = this.client.embed({
      title: 'About',
      description: 'Chatbot made for the ECU SE 19 Discord.',
      fields: [
        {
          name: 'Author',
          value: '<@152093238449274880>',
          inline: true,
        },
        {
          name: 'Uptime',
          value: uptime,
          inline: true,
        },
        {
          name: 'Usage',
          value: `${Math.round(used * 100) / 100} MB`,
          inline: true,
        },
      ],
    });
    message.channel.send('', { embed });
  }
}

module.exports = AboutCommand;
