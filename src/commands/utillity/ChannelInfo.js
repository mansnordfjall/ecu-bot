const Command = require('../../structs/Command');

class ChannelCommand extends Command {
  constructor(handler) {
    super(handler, 'channel', {
      group: 'util',
      aliases: ['channelinfo'],
      description: 'Shows information about a channel on this server.',
      examples: ['channel', 'channel #general'],
      args: [
        {
          name: 'channel',
          type: 'channel',
          optional: true,
        },
      ],
    });
  }

  async invoke(message, args) {
    let channel;
    if (!args.channel) { channel = message.channel; } else channel = args.channel;

    if (channel.constructor === Array) {
      const channelStr = args.channel.map((c, i) => `**${i + 1}.** \`${c.type}:${c.name}\``).join(', ');
      message.reply(`Found multiple matches (choose number) \n${channelStr}`);

      const filter = (m) => {
        if (m.author.id === message.author.id) {
          if (!isNaN(m.content)) {
            const input = parseInt(m.content, 10) - 1;
            if (input <= args.channel.length || input > 0) { return true; }
            return false;
          } return false;
        } return false;
      };

      try {
        const collected = await message.channel.awaitMessages(filter, { max: 1, time: 15000, errors: ['time'] });
        channel = args.channel[parseInt(collected.first(), 10) - 1];
      } catch (err) {
        if (err) message.reply('the command was canceled!');
        return undefined;
      }
    }

    let channelData = '';
    if (channel.type === 'text') { channelData = `\n**Message cache size**: \`${channel.messages.size}\``; } else if (channel.type === 'voice') { channelData = `\n**In voice**: \`${channel.members.size}\``; }

    const embed = this.client.embed({
      title: `Channel Info | ${channel.name}`,
      description: `${channel.type === 'text' ? `**Link**: ${channel}` : ''}`
        + `\n**Created**: ${channel.createdAt}`
        + `\n**Type**: \`${channel.type}\``
        + `${channelData}`,
    });

    message.channel.send({ embed });
    return undefined;
  }
}

module.exports = ChannelCommand;
