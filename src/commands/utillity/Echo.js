const Command = require('../../structs/Command');

class EchoCommand extends Command {
  constructor(handler) {
    super(handler, 'echo', {
      group: 'util',
      aliases: ['say', 'repeat'],
      description: 'Echo\'s the phrase that was passed as parameter.',
      examples: ['echo Hello World!'],
      args: [
        {
          name: 'phrase',
          remainder: true,
          optional: true,
        },
      ],
    });
  }

  invoke(message, args) {
    let { phrase } = args;
    if (!phrase) phrase = '\\**says nothing* \\*';
    message.channel.send(phrase);
  }
}

module.exports = EchoCommand;
