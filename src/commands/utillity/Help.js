const Command = require('../../structs/Command');

class HelpCommand extends Command {
  constructor(handler) {
    super(handler, 'help', {
      group: 'util',
      aliases: ['commands'],
      description: 'Shows a list of available commands.',
      examples: ['help', 'help ping'],
      guildOnly: false,
      args: [
        {
          name: 'command',
          optional: true,
        },
      ],
    });
  }

  invoke(message, args) {
    let embed;

    if (!args.command) {
      const { groups } = this.registry;
      const groupStrings = [];

      const prettyGroupNames = this.client.commandGroupNames;

      for (const group of Object.keys(prettyGroupNames)) {
        if (groups.has(group)) {
          const groupCommands = this.registry.commands.filter((cmd) => cmd.group === group)
            .map((cmd) => `\`${cmd.name}\` - ${cmd.description}`).join('\n');
          groupStrings.push(`__**${prettyGroupNames[group]}**__\n${groupCommands}`);
        }
      }

      embed = this.client.embed({
        title: 'Command List',
        description: groupStrings.join('\n'),
        footer: { text: 'Use "help <command>" for more detailed info' },
      });

      message.channel.send({ embed });
    } else {
      const command = this.registry.get(args.command);
      if (!command || command.hidden) {
        message.reply(`Could not find a command with the name: \`${args.command}\`.`);
        return undefined;
      }

      const aliases = command.aliases ? command.aliases.map((alias) => `\`${alias}\``).join(', ') : undefined;
      const examples = command.examples ? command.examples.map((exs) => `\n  \`${exs}\``).join('') : undefined;
      const params = command.args && command.args.length > 0
        ? command.args.map((arg, i) => `\n  Position: \`${i + 1}\` : \`${arg.optional ? `[${arg.name}]` : arg.name}\``).join('') : undefined;

      embed = this.client.embed({
        title: `Command | ${command.name}`,
        description: `**Description**: ${command.description}`
          + `${aliases ? `\n**Aliases**: ${aliases}` : ''}`
          + `${examples ? `\n**Examples**: ${examples}` : ''}`
          + `${params ? `\n**Arguments**: ${params}` : ''}`,
      });

      message.channel.send({ embed });
    }

    return undefined;
  }
}

module.exports = HelpCommand;
