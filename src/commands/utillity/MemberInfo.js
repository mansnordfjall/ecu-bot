const Command = require('../../structs/Command');

class MemberCommand extends Command {
  constructor(handler) {
    super(handler, 'member', {
      group: 'util',
      aliases: ['memberinfo'],
      description: 'Shows information about a member on this server.',
      examples: ['member', 'member rvn#0002'],
      args: [
        {
          name: 'member',
          type: 'guildmember',
          optional: true,
          remainder: true,
        },
      ],
    });
  }

  invoke(message, args) {
    const member = args.member ? args.member : message.member;
    const roles = member.roles.map((role) => `\`${role.name}\``).join(', ');
    const embed = this.client.embed({
      title: `Member Info | ${member.user.tag}`,
      description: `**Roles**: ${roles}`
        + `\n**Joined**: ${member.joinedAt}`
        + `\n**Created**: ${member.user.createdAt}`,
    });

    message.channel.send({ embed });
  }
}

module.exports = MemberCommand;
