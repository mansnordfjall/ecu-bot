const Command = require('../../structs/Command');

class PingCommand extends Command {
  constructor(handler) {
    super(handler, 'ping', {
      group: 'util',
      aliases: ['latency'],
      description: 'Shows the bots ping in milliseconds.',
      guildOnly: false,
    });
  }

  invoke(message) {
    message.channel.send('Pinging...')
      .then((sentMsg) => {
        sentMsg.edit(`:ping_pong: Pong! Took \`${sentMsg.createdTimestamp - message.createdTimestamp}ms\``);
      });
  }
}

module.exports = PingCommand;
