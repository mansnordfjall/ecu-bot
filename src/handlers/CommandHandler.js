const { existsSync } = require('fs');
const glob = require('glob');
const CommandRegistry = require('../structs/CommandRegistry');
const CommandParser = require('./CommandParser');
const { ParserResult, Emojis } = require('../util/Constants');

/**
 * Handles command related things.
 * @class CommandHandler
 * @type {CommandHandler}
 */
class CommandHandler {
  constructor(client) {
    this.client = client;
    this.registry = new CommandRegistry(this);
    this.parser = new CommandParser(this);
  }

  load(filepath) {
    if (!existsSync(filepath)) {
      this.client.logger.error(`A file with the path "${'hello'}" does not seem to exist.`);
      return undefined;
    }

    // eslint-disable-next-line global-require, import/no-dynamic-require
    const cmd = require(filepath);
    let Clazz;

    if (cmd && Object.getPrototypeOf(cmd).name !== 'Command') {
      for (const k of Object.keys(cmd)) {
        if (Object.getPrototypeOf(cmd[k].name === 'Command')) {
          Clazz = cmd[k];
          break;
        }
      }
    } else Clazz = cmd;

    const command = new Clazz(this);
    command.filepath = filepath;

    return command;
  }

  loadCommands() {
    const files = [];

    if (!this.client.commandDirectory) return false;
    if (!existsSync(this.client.commandDirectory)) {
      this.client.logger.error(`The given command path does not seem to exist! "${this.client.commandDirectory}"`);
      return undefined;
    }

    files.push(...glob.sync(`${this.client.commandDirectory}/**/*.js`));

    for (const file of files) {
      if (require.cache[require.resolve(file)]) delete require.cache[require.resolve(file)];
      const command = this.load(file);
      if (command) this.registry.add(command);
    }

    this.client.logger.debug('Loaded all commands!');
    return this.registry.commands;
  }

  isMentionPrefix(content) {
    const regex = new RegExp(`^(<@)(!?)(${this.client.user.id}>\\s+)`);
    return regex.test(content);
  }

  handle(message) {
    if (message.author.bot) return undefined;

    let parseInfo;

    // Check if message is a command
    if (message.content.startsWith(this.client.commandPrefix)) {
      parseInfo = this.parser.getInfo(message);
    } else if (this.isMentionPrefix(message.content)) {
      message.mentions.users.delete(this.client.user.id);
      message.mentions.members.delete(this.client.user.id);
      parseInfo = this.parser.getInfo(message, true);
    } else return undefined;

    // If it's not a command we pass it to the other handlers
    if (!parseInfo.command) {
      if (message.channel.type === 'text') {
        const isTag = this.client.tagHandler.handle(parseInfo.commandName, message.channel);
        if (isTag) return undefined;
      }
    }

    // Handle command parse result
    if (parseInfo.result === ParserResult.OK) {
      parseInfo.command.invoke(message, parseInfo.args);
    } else if (parseInfo.result === ParserResult.UNKNOWN_COMMAND) {
      message.react(Emojis.error);
    } else if (parseInfo.result === ParserResult.GUILD_ONLY) {
      message.reply('This is a guild only command!');
    } else if (parseInfo.result === ParserResult.MISSING_PERMISSIONS) {
      message.reply('You don\'t have the permissions required to execute this command!');
    } else if (parseInfo.result === ParserResult.COMMAND_ERROR) {
      message.reply(parseInfo.error);
    } else {
      message.channel.send('Something unexpected happened! Try again.'
        + `\n\`\`\`\n${parseInfo.error}\n\`\`\``
        + `\nIf this problem persists contact ${this.client.ownerMention()}.`);
    }

    return undefined;
  }
}

module.exports = CommandHandler;
