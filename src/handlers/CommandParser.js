const { ParserResult } = require('../util/Constants');
const DefaultType = require('./parserTypes/DefaultType');
const GuildMemberType = require('./parserTypes/GuildMemberType');
const ChannelType = require('./parserTypes/ChannelType');
const BooleanType = require('./parserTypes/BooleanType');
const NumberType = require('./parserTypes/NumberType');
const RoleType = require('./parserTypes/RoleType');
const ListType = require('./parserTypes/ListType');

class CommandParser {
  constructor(handler) {
    this.handler = handler;
    this.types = {
      DEFAULT: DefaultType,
      GUILDMEMBER: GuildMemberType,
      CHANNEL: ChannelType,
      BOOLEAN: BooleanType,
      NUMBER: NumberType,
      ROLE: RoleType,
      LIST: ListType,
    };
  }

  get client() {
    return this.handler.client;
  }

  get registry() {
    return this.handler.registry;
  }

  parseArgType(message, type, arg, index) {
    const typeUC = type.toUpperCase();
    for (const key of Object.keys(this.types)) {
      if (typeUC === key) {
        return this.types[key](message, arg, index);
      }
    }
    return this.types.DEFAULT(message, arg, index);
  }

  getArgsMention(content) {
    return content.split(/\s+/g).slice(2);
  }

  getArguments(message, command, mentionTrigger) {
    if (!command.args || command.args.length === 0) { return {}; }

    // Split args passed in message
    let argArray;
    if (mentionTrigger) argArray = this.getArgsMention(message.content);
    else argArray = message.content.split(/\s+/g).slice(1);

    const argObj = {}; // Object that will contain the arg properties and their parsed values

    for (let i = 0; i < command.args.length; i++) {
      const arg = command.args[i];
      let value = argArray[i];

      // eslint-disable-next-line no-continue
      if (arg.optional && !value) continue;

      if (arg.remainder) {
        argArray.splice(0, i);
        value = argArray.join(' ');
      }

      // Pass argument to type parser and set it's value on "argObj"
      const type = arg.type ? arg.type : 'string';
      const parse = this.parseArgType(message, type, value, i);
      argObj[arg.name] = parse.value;

      // Return if an error occurred
      if (parse.error) { return parse.error; }
    }
    // If no error occurred then return the parsed args object
    return argObj;
  }

  /**
   * Get the command from the message.
   * @param {Message} message The message that invoked the command.
   * @param {string} prefix The prefix used.
   * @param {boolean} mentionTrigger If the invoke was triggered by a mention.
   * @returns {Command|Object}
   */
  getCommand(message, prefix, mentionTrigger) {
    let commandName;
    if (mentionTrigger) commandName = message.content.split(/\s+/g)[1].toLowerCase();
    else commandName = message.content.split(/\s+/g)[0].slice(prefix.length).toLowerCase();

    return {
      name: commandName,
      command: this.registry.get(commandName),
    };
  }

  /**
   * Get the info passed in the message.
   * @param {Message} message The message that invoked the command.
   * @param {boolean} mentionTrigger If the command was invoked by a mention.
   * @returns {Object}
   */
  getInfo(message, mentionTrigger = false) {
    const { name, command } = this.getCommand(message, this.client.commandPrefix, mentionTrigger);

    // The base parse object to return
    const parseResult = {
      result: ParserResult.OK,
      commandName: name,
      command,
      args: {},
      error: undefined,
    };

    // Make sure a command was found, otherwise return command not found
    if (!command) {
      parseResult.result = ParserResult.UNKNOWN_COMMAND;
      return parseResult;
    }

    // Check if the command can only be executed by bot owner
    if (command.ownerOnly && !this.client.config.ownerIds.includes(message.author.id)) {
      parseResult.result = ParserResult.MISSING_PERMISSIONS;
      return parseResult;
    }

    // Check if the command is guild only
    if (command.guildOnly && message.channel.type !== 'text') {
      parseResult.result = ParserResult.GUILD_ONLY;
      return parseResult;
    }

    // Get / Parse the message
    const args = this.getArguments(message, command, mentionTrigger);

    // Check if parse returned an error
    if (typeof args === 'string') {
      parseResult.result = ParserResult.COMMAND_ERROR;
      parseResult.error = args;
      return parseResult;
    }

    // Everything went fine, so retrun the result
    parseResult.args = args;
    parseResult.result = ParserResult.OK;
    return parseResult;
  }
}

module.exports = CommandParser;
