function getValue(message, arg, index) {
  const data = {
    value: null,
    error: null,
  };

  const validTruthy = ['true', 'on', 'enable', 'enabled', '1'];
  const validFalsy = ['false', 'off', 'disable', 'disabled', '0'];

  if (validTruthy.includes(arg)) {
    data.value = true;
    return data;
  }

  if (validFalsy.includes(arg)) {
    data.value = false;
    return data;
  }

  data.error = `Argument ${index > 0 ? `\`${index + 1}\` ` : ''}is not a valid boolean`;
  return data;
}

module.exports = getValue;
