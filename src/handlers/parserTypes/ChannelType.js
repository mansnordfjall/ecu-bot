function getValue(message, arg, index) {
  const ChannelRegex = /^(<#[0-9]{18}\s?>)|^([0-9]{18})|^(.{1,100})/g;
  const matches = ChannelRegex.exec(arg);

  const data = {
    value: null,
    error: null,
  };

  if (matches) {
    if (matches[1]) { // mention
      const channelId = arg.split(/<#([0-9]{18})>/g)[1];
      data.value = message.guild.channels.get(channelId);
    } else if (matches[2]) { // id
      if (message.guild.channels.has(arg)) { data.value = message.guild.channels.get(arg); }
    } else if (matches[3]) { // channel name
      const channel = message.guild.channels.filter((c) => c.name === arg);
      if (channel.size > 1) { data.value = channel.array(); } else data.value = channel.first();
    }

    if (!data.value) { data.error = 'Couldn\'t find a channel matching the criteria.'; }
    return data;
  }

  data.error = `Argument ${index > 0 ? `\`${index + 1}\` ` : ''}has to be a valid role resolvable.`;
  return data;
}

module.exports = getValue;
