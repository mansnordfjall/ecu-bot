function getValue(message, arg, index) {
  const data = {
    value: arg,
    error: null,
  };

  if (!data.value || data.value.length === 0) { data.error = `Invalid argument, no data passed at argument \`${index + 1}\`.`; }

  return data;
}

module.exports = getValue;
