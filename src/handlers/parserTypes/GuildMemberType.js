function getValue(message, arg, index) {
  const MemberRegex = /^(<@!?[0-9]{18}>\s?)|^([0-9]{18})|^(.{1,32}#[0-9]{4})/g;
  const matches = MemberRegex.exec(arg);

  const data = {
    value: null,
    error: null,
  };

  if (matches) {
    if (matches[1]) { // mention
      const memberId = arg.split(/<@!?([0-9]{18})>/g)[1];
      data.value = message.guild.members.get(memberId);
    } else if (matches[2]) { // id
      data.value = message.guild.members.get(arg);
    } else if (matches[3]) { // tag
      data.value = message.guild.members.find((m) => m.user.tag === arg);
    }

    if (!data.value) { data.error = 'Couldn\'t find a member matching the criteria.'; }
    return data;
  }

  data.error = `Argument ${index > 0 ? `\`${index + 1}\` ` : ''}has to be a valid member resolvable.`;
  return data;
}

module.exports = getValue;
