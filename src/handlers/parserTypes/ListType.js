function getValue(message, arg, index) {
  const data = {
    value: null,
    error: null,
  };

  if (arg.length === 0) { data.error = `Argument ${index > 0 ? `\`${index + 1}\` ` : ''}is not a valid list.`; }

  const sep = arg.separator ? arg.seperator : ',';
  data.value = arg.split(sep).map((val) => val.trim());

  return data;
}

module.exports = getValue;
