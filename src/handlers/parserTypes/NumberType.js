function getValue(message, arg, index) {
  const data = {
    value: null,
    error: null,
  };

  if (isNaN(arg)) {
    data.error = `Argument ${index > 0 ? `\`${index + 1}\` ` : ''}is not a number!`;
    return data;
  }

  data.value = Number(arg);
  return data;
}

module.exports = getValue;
