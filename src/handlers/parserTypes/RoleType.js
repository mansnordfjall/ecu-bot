function getValue(message, arg, index) {
  const RoleRegex = /^(<@&[0-9]{18}>\s?)|^([0-9]{18})|^(.{1,100})/g;
  const matches = RoleRegex.exec(arg);

  const data = {
    value: arg,
    error: null,
  };

  if (matches) {
    if (matches[1]) { // mention
      const roleId = arg.split(/<@&([0-9]{18})>/g)[1];
      data.value = message.guild.roles.get(roleId);
    } else if (matches[2]) { // id
      data.value = message.guild.roles.get(arg);
    } else if (matches[3]) { // name
      data.value = message.guild.roles.find((r) => r.name === arg);
    }

    if (!data.value) data.error = 'Couldn\'t find a role matching the criteria.';
    return data;
  }

  data.error = `Argument ${index > 0 ? `\`${index + 1}\` ` : ''}has to be a valid role resolvable.`;
  return data;
}

module.exports = getValue;
