const fs = require('fs');

class FSProvider {
  /**
   * Dump object to a JSON file.
   * @param {Object} data Data to dump to "data/"
   * @param {string} filename The file to dump to.
   * @param {boolean} indented If the JSON content should be formatted.
   * @returns {Promise<void>}
   */
  static dumpJSON(data, filename, indented = false) {
    return new Promise((resolve, reject) => {
      try {
        const json = indented
          ? JSON.stringify(data, null, 4) : JSON.stringify(data);

        fs.writeFile(filename, json, (err) => {
          if (err) reject(err);
          resolve();
        });
      } catch (err) {
        reject(err);
      }
    });
  }
}

module.exports = FSProvider;
