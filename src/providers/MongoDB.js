const { MongoClient } = require('mongodb');

const { MONGO_USER, MONGO_PASS } = process.env;
const { devMode } = require('../../config.json');
const { URL } = require('../util/Constants');

class Database {
  constructor(client) {
    this.dbUrl = URL.db(MONGO_USER, MONGO_PASS);
    this.client = client;
  }

  /**
   * Connect to the mongo database.
   * @returns {Promise<*>}
   */
  connect() {
    return new Promise((resolve, reject) => {
      MongoClient.connect(this.dbUrl)
        .then((client) => {
          this.dbClient = client;
          this.db = client.db(devMode ? 'dev' : 'prod');
          resolve(1);
        })
        .catch(reject);
    });
  }

  /**
   * Close the connection to the mongo database.
   * @returns {Promise<*>}
   */
  close() {
    return new Promise((resolve, reject) => {
      this.dbClient.close()
        .then(resolve)
        .catch(reject);
    });
  }

  /**
   * Change the active database.
   * @param {string} dbName Name of the database to change to.
   * @returns {Promise<*>}
   */
  changeDb(dbName) {
    this.db = this.dbClient.db(dbName);
    return Promise.resolve(1);
  }

  /**
   * Insert a document into collection.
   * @param {string} collectionName Name of the db collection.
   * @param {Object} data Data to insert.
   * @returns {Promise<*>}
   */
  insert(collectionName, data) {
    return new Promise((resolve, reject) => {
      this.db.collection(collectionName)
        .insertOne(data)
        .then(resolve)
        .catch(reject);
    });
  }

  /**
   * Insert one more documents into collection.
   * @param {string} collectionName Name of the db collection.
   * @param {Object[]} dataArray Array of data to insert.
   * @returns {Promsie<*>}
   */
  insertMany(collectionName, dataArray) {
    return new Promise((resolve, reject) => {
      if (dataArray.constructor !== Array) { return reject(new TypeError('Data has to be passed in an array.')); }
      this.db.collection(collectionName)
        .insertMany(dataArray)
        .then(resolve)
        .catch(reject);
      return undefined;
    });
  }

  /**
   * Push data to an array in the collection.
   * @param {string} collectionName Name of the db collection.
   * @param {Object} filter The filter to apply.
   * @param {Object} data The data to add.
   * @param {Object} [options] Additional options for the update.
   * @returns {Promise<*>}
   */
  pushToArray(collectionName, filter, data, options = null) {
    return new Promise((resolve, reject) => {
      this.db.collection(collectionName)
        .updateOne(filter, { $push: data }, options)
        .then(resolve)
        .catch(reject);
    });
  }

  /**
   * Add data to an array in the collection, if the data is already in array then do nothing.
   * @param {string} collectionName Name of the db collection.
   * @param {Object} filter The filter to apply.
   * @param {Object} data The data to add.
   * @param {Object} [options] Additional options for the update.
   * @returns {Promise<*>}
   */
  addToArrayUnique(collectionName, filter, data, options = null) {
    return new Promise((resolve, reject) => {
      this.db.collection(collectionName)
        .updateOne(filter, { $addToSet: data }, options)
        .then(resolve)
        .catch(reject);
    });
  }

  /**
   * Remove value(s) from an array.
   * @param {string} collectionName Name of the db collection.
   * @param {Object} filter The filter to apply.
   * @param {Object} data The data to add.
   * @param {Object} options Additional options for the update.
   * @returns {Promise<*>}
   */
  removeFromArray(collectionName, filter, data, options = null) {
    return new Promise((resolve, reject) => {
      this.db.collection(collectionName)
        .updateMany(filter, { $pull: data }, options)
        .then(resolve)
        .catch(reject);
    });
  }

  /**
   * Find a document in the collection.
   * @param {string} collectionName Name of the db collection.
   * @param {Object} query The query to use.
   * @returns {Promise<Object>}
   */
  find(collectionName, query) {
    return new Promise((resolve, reject) => {
      this.db.collection(collectionName)
        .findOne(query)
        .then(resolve)
        .catch(reject);
    });
  }

  /**
   * Find a document and return only the specified fields.
   * @param {string} collectionName Name of the db collection.
   * @param {Object} query The query to use.
   * @param {Object} fields The fields to return.
   * @returns {Promise<Object>}
   */
  findProjection(collectionName, query, fields) {
    return new Promise((resolve, reject) => {
      this.db.collection(collectionName)
        .findOne(query, { projection: fields })
        .then(resolve)
        .catch(reject);
    });
  }

  /**
   * Returns all the documents in a collection.
   * @param {string} collectionName Name of the db collection.
   * @returns {Promise<Object[]>}
   */
  findAll(collectionName) {
    return new Promise((resolve, reject) => {
      this.db.collection(collectionName)
        .find({}).toArray()
        .then(resolve)
        .catch(reject);
    });
  }

  /**
   * Find a document or create it if it wasn't found.
   * @param {string} collectionName Name of the db collection.
   * @param {Object} filter The filter to apply.
   * @param {Object} data The data to insert if there is not document.
   * @returns {Promise<Object>}
   */
  findOrInsert(collectionName, filter, data) {
    return new Promise((resolve, reject) => {
      this.db.collection(collectionName)
        .findOneAndUpdate(filter, {
          $setOnInsert: data,
        }, { upsert: true, returnOriginal: false })
        .then(resolve)
        .catch(reject);
    });
  }

  /**
   * Update a document that meets a given criteria.
   * @param {string} collectionName Name of the db collection.
   * @param {Object} filter The filter to apply.
   * @param {Object} data The data to update.
   * @returns {Promise<*>}
   */
  update(collectionName, filter, data) {
    return new Promise((resolve, reject) => {
      this.db.collection(collectionName)
        .updateOne(filter, {
          $set: data,
        })
        .then(resolve)
        .catch(reject);
    });
  }

  /**
   * Update one or more documents that meet a given criteria.
   * @param {string} collectionName Name of the db collection.
   * @param {Object} filter The filter to use.
   * @param {Object[]} dataArray Array of data to update.
   * @returns {Promise<*>}
   */
  updateMany(collectionName, filter, dataArray) {
    return new Promise((resolve, reject) => {
      this.db.collection(collectionName)
        .updateMany(filter, {
          $set: dataArray,
        })
        .then(resolve)
        .catch(reject);
    });
  }

  /**
   * Index a field in a document.
   * @param {string} collectionName Name of the db collection.
   * @param {Object} field Field to index.
   * @returns {Promise<*>}
   */
  index(collectionName, field) {
    return new Promise((resolve, reject) => {
      this.db.collection(collectionName)
        .createIndex(field)
        .then(resolve)
        .catch(reject);
    });
  }

  /**
   * Delete a document.
   * @param {string} collectionName Name of the db collection.
   * @param {Object} filter The filter to use.
   * @returns {Promise<*>}
   */
  delete(collectionName, filter) {
    return new Promise((resolve, reject) => {
      this.db.collection(collectionName)
        .deleteOne(filter)
        .then(resolve)
        .catch(reject);
    });
  }
}

module.exports = Database;
