class TagHandler {
  constructor(client) {
    this.client = client;
  }

  /**
   * Handle the incoming message and check for tags etc.
   * @param {string} tagName The name of the tag typed.
   * @param {TextChannel} channel The text channel the message was sent in.
   * @returns {boolean} If it was handeled successfully.
   */
  handle(tagName, channel) {
    const content = this.client.tagRegistry.get(tagName);
    if (content) {
      channel.send(content);
      return true;
    }
    return false;
  }
}

module.exports = TagHandler;
