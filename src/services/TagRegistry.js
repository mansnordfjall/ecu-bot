const fs = require('fs');
const path = require('path');
/**
 * This is just a temporary solution until I can fix a database to handle the tags system
 */
class TagRegistry {
  constructor(client) {
    /** Parent client */
    this.client = client;
    this.tags = {}; // object with all tags
  }

  load() {
    const dataPath = path.resolve(this.client.tagsPath, '../');
    if (!fs.existsSync(dataPath)) {
      fs.mkdirSync(dataPath);
      if (!fs.existsSync(this.client.tagsPath)) { fs.writeFileSync(this.client.tagsPath, '{}'); }
    }

    const content = fs.readFileSync(this.client.tagsPath);
    const values = JSON.parse(content);
    this.tags = values;
  }

  save() {
    const json = JSON.stringify(this.tags, null, 4);
    fs.writeFileSync(this.client.tagsPath, json);
  }

  get(tagName) {
    if (!this.tags[tagName]) return null;
    return this.tags[tagName];
  }

  /**
   * Add a non-existing tag to the tags.
   * @param {string} tagName The name of the tag to add.
   * @param {string} tagContent The content to store in the tag.
   * @returns {boolean} Whether the add was successful or not.
   */
  add(tagName, tagContent) {
    if (this.tags[tagName]) return false;
    this.tags[tagName] = tagContent;
    this.save();
    return true;
  }

  remove(tagName) {
    if (!this.tags[tagName]) return false;
    delete this.tags[tagName];
    this.save();
    return true;
  }
}

module.exports = TagRegistry;
