const path = require('path');
const { Client } = require('discord.js');
const CommandHandler = require('../handlers/CommandHandler');
const TagRegistry = require('../services/TagRegistry');
const TagHandler = require('../services/TagHandler');
const Logger = require('./Logger');

class BotClient extends Client {
  /**
   * @param {BotClientOptions} options Options for the BotClient client.
   * @param {ClientOptions} djsOptions DiscordJS Client options.
   */
  constructor(options = {}, djsOptions = {}) {
    super(djsOptions);

    const {
      config = null,
      commandPrefix = '!',
      commandDirectory = null,
      commandGroupNames = {},
      tagsPath = path.resolve(__dirname, '../../data/tags.json'),
      logDirectory = path.resolve(__dirname, '../../logs'),
    } = options;

    /**
     * Object containing config properties.
     * @type {Config}
     */
    this.config = config;
    /**
     * The prefix to use for recognizing commands.
     * @type {string}
     */
    this.commandPrefix = commandPrefix;
    /**
     * The directory to use for loading commands.
     * @type {string}
     */
    this.commandDirectory = commandDirectory;
    /**
     * What full names to associate with each group.
     * @type {Object}
     */
    this.commandGroupNames = commandGroupNames;
    /**
     * The path for the file that stores all the tags. (temporary)
     * @type {string}
     */
    this.tagsPath = tagsPath;
    /**
     * The directory to use for outputting log files.
     * @type {string}
     */
    this.logDirectory = logDirectory;
    /**
     * The logger.
     * @type {Logger}
     */
    this.logger = new Logger(this, this.logDirectory);
  }

  /**
   * Returns a new embed object with some predefined properties.
   * @param {Object} embedData Embed properties.
   * @returns {Object}
   */
  embed(embedData) {
    return Object.assign(embedData, {
      color: 0xFFFFFF,
    });
  }

  /**
   * Get the mention of the owner.
   * @param {number} [index = 0] The index from the id array.
   * @returns {string} Owner mention string.
   */
  ownerMention(index = 0) {
    return `<@${this.config.ownerIds[index]}>`;
  }

  /**
   * Login
   * @param {string} token The token to use.
   */
  login(token) {
    super.login(token)
      .catch((error) => this.logger.error(error));

    this.once('ready', () => {
      this.logger.info('Hello World!');

      if (!this.commandHandler) { this.commandHandler = new CommandHandler(this); }

      this.commandHandler.loadCommands();

      if (!this.tagRegistry) { this.tagRegistry = new TagRegistry(this); }

      if (!this.tagHandler) { this.tagHandler = new TagHandler(this); }

      this.tagRegistry.load();

      if (!this.user.presence.game) {
        this.user.setActivity(this.config.activity.msg, { type: this.config.activity.type });
      }

      this
        .on('message', (message) => this.commandHandler.handle(message))
        .on('messageUpdate', (oldMessage, newMessage) => this.commandHandler.handle(newMessage));
    });
  }
}

/**
 * A Config object.
 * @typedef {Object} Config
 * @property {string} logChannel Discord channel to output logs to.
 * @property {string} ownerIds The Discord id of the bot owner(s).
 * @property {{type, msg}} activity The bot activity details to use.
 */

/**
 * BotClient options.
 * @typedef {Object} BotClientOptions
 * @property {Config} config Config object to use.
 * @property {string} commandDirectory Command directory to use.
 * @property {Object} commandGroupNames The names to associate with each group.
 * @property {string} logDirectory Directory to output log files in.
 */

module.exports = BotClient;
