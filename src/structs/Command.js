/* eslint-disable no-unused-vars */
// Used to provide intellisense for these types.
const Client = require('../structs/Client');
const CommandHandler = require('../handlers/CommandHandler');
const CommandRegistry = require('../structs/CommandRegistry');
/* eslint-enable no-unused-vars */

/**
 * Represents a command.
 */
class Command {
  /**
   * @param {CommandHandler} handler The CommandHandler that should handle this command.
   * @param {string} commandName The name to use for this command.
   * @param {CommandConfig} config The command config.
   */
  constructor(handler, commandName, config = {}) {
    /**
     * The CommandHandle that will handle this command.
     * @type {external:CommandHandler}
     */
    this.handler = handler;

    const {
      group = 'none',
      description = 'No description available.',
      aliases = null,
      examples = null,
      args = null,
      guildOnly = true,
      ownerOnly = false,
      permission = null,
      cooldown = null,
      hidden = false,
    } = config;

    /**
     * The name of this command.
     * @type {string}
     */
    this.name = commandName;
    /**
     * The command group this command belongs to.
     * @type {string}
     */
    this.group = group;
    /**
     * A description for this command.
     * @type {string}
     */
    this.description = description;
    /**
     * The aliases of this command (if it has any)
     * @type {string[]}
     */
    this.aliases = aliases;
    /**
     * Examples on command usage.
     * @type {string[]}
     */
    this.examples = examples;
    /**
     * The arguments for this command.
     * @type {CommandArgument[]}
     */
    this.args = args;
    /**
     * If this is a guild only command.
     * @type {boolean}
     */
    this.guildOnly = guildOnly;
    /**
     * If this command is available only to the bot owner.
     * @type {boolean}
     */
    this.ownerOnly = ownerOnly;
    /**
     * The lowest permission required to execute this command.
     * @type {number}
     */
    this.permission = permission;
    /**
     * The per member cooldown for this command.
     * @type {number}
     */
    this.cooldown = cooldown;
    /**
     * If this command is hidden from the help list.
     * @type {boolean}
     */
    this.hidden = hidden;
    /**
     * This file location of this command.
     * @type {string}
     */
    this.filepath = null;
  }

  /**
   * The client that instantiated this command.
   * @type {Client}
   */
  get client() {
    return this.handler.client;
  }

  /**
   * The CommandRegistry that this command is attatched to.
   * @type {CommandRegistry}
   */
  get registry() {
    return this.handler.registry;
  }

  /**
   * Called when the command is invoked.
   * @param {Message} message The message that invoked the command.
   * @param {Object<any>} args The arguments passed in the message.
   */
  invoke(message, args) { } // eslint-disable-line no-empty-function, no-unused-vars

  validateProperties() {
    if (this.args) {
      if (this.args.constructor !== Array) {
        this.client.logger.error(`Arguments property has to be an array. (in "${this.name}" command)`);
        return false;
      } if (this.args.length === 0) {
        this.client.logger.error(`No arguments were given to the arguments array. (in "${this.name}" command)`);
        return false;
      }

      let errorPosStr;
      let lastOptional = false;
      for (let i = 0; i < this.args.length; i++) {
        const arg = this.args[i];
        errorPosStr = `("${this.name}" command at argument "${++i}")`;

        if (!arg.name) {
          this.client.logger.error(`Command arguments must contain a "name" property! ${errorPosStr}`);
          return false;
        }
        if (arg.separator && arg.type !== 'list') {
          this.client.logger.error('"separator" property is only valid when the argument type is a list!'
            + ` ${errorPosStr}`);
          return false;
        }
        if (arg.optional) {
          if (typeof arg.optional !== 'boolean') {
            this.client.logger.error(`"optional" argument parameter needs to be a boolean. ${errorPosStr}`);
            return false;
          }
          lastOptional = true;
        } else if (lastOptional) {
          this.client.logger.error(`"optional" arguments must not be place before required arguments! ${errorPosStr}`);
          return false;
        }
        if (arg.remains) {
          if (typeof arg.remains !== 'boolean') {
            this.client.logger.error(`"remains" argument parameter needs to be a boolean. ${errorPosStr}`);
            return false;
          } if (i !== --this.args.length) {
            this.client.logger.error(`"remains" property have to be the last argument in a command. ${errorPosStr}`);
            return false;
          }
        }
      }
    }
    return true;
  }

  /**
   * Reloads this command.
   * @returns {boolean} Whether the command was reloaded successfully.
   */
  reload() {
    return this.handler.registry.reload(this);
  }
}

/**
 * A command argument.
 * @typedef {Object} CommandArgument
 * @property {string} name The name used to refer to this argument.
 * @property {('guildmember')|('channel')|('role')|('number')|('string')|('list')} type
 * The type to use for this argument.
 * @property {string|RegExp} [separator=' '] The separator to use for lists.
 * @property {boolean} [optional=false] If this argument is optional or not.
 * @property {boolean} [remainder=false] If this argument should join the rest after this arg.
 */

/**
 * An object defining command properties.
 * @typedef {Object} CommandConfig
 * @property {string} group The command group this command belongs to.
 * @property {string} description A description of this command.
 * @property {string[]} aliases The aliases this command could go by.
 * @property {string[]} examples Examples of command usage.
 * @property {CommandArgument[]} args The arguments for this command.
 * @property {boolean} guildOnly If this command should be available in guilds only.
 * @property {boolean} ownerOnly If this command can only be executed by bot owner.
 * @property {PermissionLevel} permission The lowest permission level needed to invoke this command.
 * @property {number} cooldown The per member cooldown this command should have.
 * @property {boolean} hidden If this command should be hidden from the help list.
 */

module.exports = Command;
