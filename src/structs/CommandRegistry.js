const { Collection } = require('discord.js');

class CommandRegistry {
  constructor(handler) {
    this.handler = handler;

    /**
     * A collection of all the commands.
     * @type {Map<string, Command|Object>}
     */
    this.commands = new Collection();
    /**
     * A collection of aliases to commands.
     * @type {Map<string, string>}
     */
    this.aliases = new Map(); // Map<string, string>
    /**
     * A collection containing the command groups.
     * @type {Map<string, string[]>}
     */
    this.groups = new Map(); // Map<string, string[]>
  }

  get client() {
    return this.handler.client;
  }

  get(commandName) {
    let command;
    if (this.commands.has(commandName)) {
      command = this.commands.get(commandName);
    } else if (this.aliases.has(commandName)) {
      const cmdName = this.aliases.get(commandName);
      command = this.commands.get(cmdName);
    }

    if (!command) return undefined;
    return command;
  }

  add(command) {
    const commandObj = command;
    const commandName = commandObj.name.toLowerCase();

    if (this.commands.has(commandName)) {
      this.client.logger.warn(`[REGISTRY] Command with the name "${commandName}" already exists!`);
      return undefined;
    }

    if (!commandObj.validateProperties()) { return undefined; }

    commandObj.handler = this.handler;
    this.commands.set(commandName, commandObj);

    if (!this.groups.has(commandObj.group)) {
      this.groups.set(commandObj.group, []);
    } else this.groups.get(commandObj.group).push(commandObj.name);

    if (commandObj.aliases && commandObj.aliases.length > 0) {
      for (let alias of commandObj.aliases) {
        alias = alias.toLowerCase();
        if (!this.aliases.has(alias)) {
          this.aliases.set(alias, commandName);
        } else {
          this.client.logger.warn(`[REGISTRY] Alias with the name "${alias}"`
            + `already exists! ("${commandName} command.")`);
        }
      }
    }

    return commandObj;
  }

  remove(commandName) {
    if (!this.commands.has(commandName)) {
      this.client.logger.warn(`[REGISTRY] There is no command named "${commandName}" to removed.`);
      return undefined;
    }

    const command = this.commands.get(commandName);

    if (command.aliases && command.aliases.length > 0) {
      for (const alias of command.aliases) {
        if (this.aliases.has(alias)) {
          this.aliases.delete(alias);
        } else {
          this.client.logger.warn(`[REGISTRY] There is no alias with the name "${alias}" mapped to this command.`);
        }
      }

      this.commands.delete(command.name);
      delete require.cache[require.resolve(command.filepath)];
      return true;
    }

    return undefined;
  }

  reload(command) {
    let commandObj;

    if (typeof command === 'string') {
      if (!this.commands.has(command)) {
        this.client.logger.warn(`[REGISTRY] There is no command named "${command}" to removed.`);
        return false;
      }
      commandObj = this.commands.get(command);
    } else commandObj = command;

    if (commandObj.aliases && commandObj.aliases.length > 0) {
      for (const alias of commandObj.aliases) {
        if (this.aliases.has(alias)) { this.aliases.delete(alias); }
      }
    }

    this.commands.delete(commandObj.name);

    if (require.cache[require.resolve(commandObj.filepath)]) {
      this.client.logger.info(commandObj.filename);
      delete require.cache[require.resolve(commandObj.filepath)];
    }

    const newCommand = this.handler.load(commandObj.filepath);
    this.client.logger.debug(`Reloading "${commandObj.name}" command.`);

    return this.add(newCommand);
  }
}

module.exports = CommandRegistry;
