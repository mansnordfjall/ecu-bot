const fs = require('fs');
const winston = require('winston');

class Logger {
  constructor(client, directory) {
    this.client = client;
    this.logDirectory = directory;

    if (!fs.existsSync(this.logDirectory)) { fs.mkdirSync(this.logDirectory); }
    const format = (opts) => `<${new Date().toLocaleString()}> [${opts.level.toUpperCase()}] ${opts.message}`;

    this.winston = new winston.Logger({
      transports: [
        new winston.transports.Console({
          timestamp: false,
          formatter: format,
          level: 'debug',
          humanReadableUnhandledException: true,
        }),
        new winston.transports.File({
          filename: `${this.logDirectory}/log`,
          level: 'warn',
          json: false,
          timestamp: false,
          formatter: format,
        }),
      ],
    });
  }

  getErrorTrace(error) {
    const source = error.stack.split('\n')[1];
    const fileInfo = source.split(/(\()(.*?)(?=\))/g)[2];

    const pathMatches = [];
    const pathRegex = /(?:(?!:).)*/g;

    let m;
    // eslint-disable-next-line no-cond-assign
    while ((m = pathRegex.exec(fileInfo)) !== null) {
      if (m.index === pathRegex.lastIndex) { pathRegex.lastIndex++; }

      for (const match of m) { pathMatches.push(match); }
    }

    const filePath = pathMatches[0] + pathMatches[2];
    const lineNumber = pathMatches[4];
    const columnNumber = pathMatches[6];

    return {
      source,
      filePath,
      lineNumber,
      columnNumber,
    };
  }

  info(message) {
    this.winston.info(message);
  }

  warn(message) {
    this.winston.warn(message);
  }

  debug(message) {
    this.winston.debug(message);
  }

  error(error) {
    if (typeof error === 'string') {
      this.winston.error(error);
      this.client.channels.get(this.client.config.logChannel).send(error);
    } else if (typeof error === 'object') {
      const errorTrace = this.getErrorTrace(error);
      const errorMsg = `"${error.message}" in `
        + `"${errorTrace.filePath} (Ln ${errorTrace.lineNumber}, Col ${errorTrace.columnNumber})"`;

      this.winston.error(errorMsg);
      this.client.channels.get(this.client.config.logChannel).send(errorMsg);
    }
  }
}

module.exports = Logger;
