module.exports = {
  ParserResult: {
    COMMAND_ERROR: -5,
    UNKNOWN_COMMAND: -4,
    MISSING_PERMISSIONS: -3,
    GUILD_ONLY: -2,
    INVALID_ARGS: -1,
    OK: 0,
  },
  Emojis: {
    error: '493825227617468416',
  },
  URL: {
    db: (user, pass) => `mongodb://${user}:${pass}@localhost:27017/?authMechanism=SCRAM-SHA-1`,
  },
};
